﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Person
    {
        public Guid Identifier { get; private set; }
        public string Name { get; private set; }
        public DateTime DateOfBirth { get; private set; }
        public TwitterHandle TwitterHandle { get; set; }
        public EmailAddress EmailAddress { get; set; }

        public Person(string name, DateTime dateOfBirth)
        {
            this.Identifier = Guid.NewGuid();
            if (null == name || string.Empty == name)
            {
                throw new ArgumentException("Person name must not be null or an empty string");
            }
            this.Name = name;
            this.DateOfBirth = dateOfBirth;
        }

        public bool IsOverEighteen()
        {
            return DateTime.UtcNow.Year - this.DateOfBirth.Year > 18;
        }
    }
}
