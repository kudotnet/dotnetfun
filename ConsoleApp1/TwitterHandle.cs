﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class TwitterHandle
    {
        private string _Handle;

        public string Handle
        {
            get => _Handle;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Do not set twitter handle to blank.");
                }
                if (value.StartsWith("@"))
                {
                    Handle = value;
                }
                else
                {
                    _Handle = string.Concat("@", value);
                }
            }
        }

        public TwitterHandle(string handle)
        {
            Handle = handle;
        }
        public string getTwitterURL()
        {
            return "https://twitter.com/" + Handle.Substring(1);
        }
    }
}
