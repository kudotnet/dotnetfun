using System;
using Xunit;
using ConsoleApp1;

namespace XUnitTestProject1
{
    public class PersonTest
    {
        [Fact]
        public void TestPersonConstructorWorks()
        {
            const string inputName = "Sarah Michelle Gellar";
            var inputDateOfBirth = new DateTime(year: 1977, month: 4, day: 14);
            var person = new Person(name: inputName, dateOfBirth: inputDateOfBirth);
            Assert.Equal(expected: person.Name, actual: inputName);
            Assert.Equal(expected: person.DateOfBirth, actual: inputDateOfBirth);
        }

        [Fact]
        public void TestPersonIsOverEighteen()
        {
            const string inputName = "Sarah Michelle Gellar";
            var inputDateOfBirth = new DateTime(year: 1977, month: 4, day: 14);
            var person = new Person(name: inputName, dateOfBirth: inputDateOfBirth);
            Assert.True(condition: person.IsOverEighteen());
        }

        [Fact]
        public void TestPersonIsNotOverEighteen()
        {
            const string inputName = "Hermione Granger";
            var inputDateOfBirth = new DateTime(year: DateTime.UtcNow.Year, month: 9, day: 19);
            var person = new Person(name: inputName, dateOfBirth: inputDateOfBirth);
            Assert.False(condition: person.IsOverEighteen());
        }

        [Fact]
        public void TestPersonNameIsNotEmpty()
        {
            var inputName = "";
            var inputDateOfBirth = new DateTime(year: 1977, month: 4, day: 14);
            var expectedArgumentException = new ArgumentException("Person name must not be null or an empty string");
            var argumentException
                = Assert.Throws<ArgumentException>(() => new Person(name: inputName, dateOfBirth: inputDateOfBirth));
            Assert.Equal(expected: expectedArgumentException.Message, actual: argumentException.Message);
        }

        [Fact]
        public void TestFreebie()
        {
            Assert.True(true);
        }

        [Fact]
        public void TestSetTwitterHandle()
        {
            var person = new Person(name: "Sarah Michelle Gellar",
                dateOfBirth: new DateTime(year: 1987, month: 4, day: 14))
            {
                TwitterHandle = new TwitterHandle("sarahmgellar")
            };
            Assert.StartsWith("@", person.TwitterHandle.Handle);
        }

        [Fact]
        public void TestRemoveTwitterHandle()
        {
            const string inputName = "Sarah Michelle Gellar";
            var inputDateOfBirth = new DateTime(year: 1977, month: 4, day: 14);
            var person = new Person(name: inputName, dateOfBirth: inputDateOfBirth)
            {
                TwitterHandle = new TwitterHandle("sarahmgellar")
            };
            var expectedArgumentException = new ArgumentException("Do not set twitter handle to blank.");
            var argumentException = Assert.Throws<ArgumentException>(() => person.TwitterHandle.Handle = "");
            Assert.Equal(expected: expectedArgumentException.Message, actual: argumentException.Message);
            Assert.Equal(expected: expectedArgumentException.Data.ToString(), actual: argumentException.Data.ToString());
        }

        [Fact]
        public void TestGetTwitterUrl()
        {
            const string inputName = "Sarah Michelle Gellar";
            var inputDateOfBirth = new DateTime(year: 1977, month: 4, day: 14);
            var person = new Person(name: inputName, dateOfBirth: inputDateOfBirth)
            {
                TwitterHandle = new TwitterHandle("sarahmgellar")
            };
            Assert.Equal(actual: "https://twitter.com/sarahmgellar", expected: person.TwitterHandle.getTwitterURL());
        }

        [Fact]
        public void TestSetEmailAddress()
        {
            var person = new Person(name: "Sarah Michelle Gellar",
                dateOfBirth: new DateTime(year: 1987, month: 4, day: 14))
            {
                EmailAddress = new EmailAddress(localPart: "sarahmichellegellar", domain: "google.com")
            };
            Assert.Equal(expected: "sarahmichellegellar@google.com", actual: person.EmailAddress.GetEmailAddressString());
        }
    }
}
